import sinon, {SinonSpy} from 'sinon';
import ApiIntegrationEventsReceiver from '../ApiIntegrationEventsReceiver'
import {
    EVENT_TYPE_COURSE_FINISHED,
    EVENT_TYPE_COURSE_SAVED_IN_COURSE_BUILDER,
    EVENT_TYPE_COURSE_STARTED_AGAIN, EVENT_TYPE_GET_PROGRESS, EVENT_TYPE_SET_PROGRESS
} from "../constants";
import Sinon from "sinon";

afterEach(() => {
    localStorage.clear();
    sinon.restore();
})

describe('flushAllListeners', () => {
    it('no listeners were registered', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');

        ApiIntegrationEventsReceiver.flushAllListeners();

        sinon.assert.notCalled(windowRemoveListenerStub);
    });

    it('some listeners were registered', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');

        let courseFinishedCallback = sinon.stub();
        let courseStartedAgainCallback = sinon.stub();
        let courseSavedInCourseBuilderCallback = sinon.stub();
        let courseGetProgressCallback = sinon.stub();
        let courseSetProgressCallback = sinon.stub();
        ApiIntegrationEventsReceiver.registerCourseFinishedEvent(courseFinishedCallback);
        ApiIntegrationEventsReceiver.registerCourseStartedAgainEvent(courseStartedAgainCallback);
        ApiIntegrationEventsReceiver.registerCourseSavedInCourseBuilderEvent(courseSavedInCourseBuilderCallback);
        ApiIntegrationEventsReceiver.registerGetProgressEvent(courseGetProgressCallback);
        ApiIntegrationEventsReceiver.registerSetProgressEvent(courseSetProgressCallback);

        ApiIntegrationEventsReceiver.flushAllListeners();

        expect(ApiIntegrationEventsReceiver.registeredListeners).toEqual({});
        sinon.assert.callCount(windowRemoveListenerStub, 5); // because there were registered 5 types of listeners
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', sinon.match.func);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', sinon.match.func);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', sinon.match.func);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', sinon.match.func);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', sinon.match.func);

        sinon.assert.notCalled(courseFinishedCallback);
        sinon.assert.notCalled(courseStartedAgainCallback);
        sinon.assert.notCalled(courseSavedInCourseBuilderCallback);
        sinon.assert.notCalled(courseGetProgressCallback);
        sinon.assert.notCalled(courseSetProgressCallback);
    });
});

it('register common types of listeners', () => {
    // @ts-ignore
    let addListenerForEventTypeStub = sinon.stub(ApiIntegrationEventsReceiver, '_addListenerForEventType');
    let courseFinishedCallback = sinon.stub();
    let courseStartedAgainCallback = sinon.stub();
    let courseSavedInCourseBuilderCallback = sinon.stub();
    let courseSetProgressCallback = sinon.stub();
    ApiIntegrationEventsReceiver.registerCourseFinishedEvent(courseFinishedCallback);
    ApiIntegrationEventsReceiver.registerCourseStartedAgainEvent(courseStartedAgainCallback);
    ApiIntegrationEventsReceiver.registerCourseSavedInCourseBuilderEvent(courseSavedInCourseBuilderCallback);
    ApiIntegrationEventsReceiver.registerSetProgressEvent(courseSetProgressCallback);

    sinon.assert.callCount(addListenerForEventTypeStub, 4);// 4 because of number of register listeners method calls
    sinon.assert.calledWith(addListenerForEventTypeStub, EVENT_TYPE_COURSE_STARTED_AGAIN, courseStartedAgainCallback);
    sinon.assert.calledWith(addListenerForEventTypeStub, EVENT_TYPE_COURSE_FINISHED, courseFinishedCallback);
    sinon.assert.calledWith(addListenerForEventTypeStub, EVENT_TYPE_COURSE_SAVED_IN_COURSE_BUILDER, courseSavedInCourseBuilderCallback);
    sinon.assert.calledWith(addListenerForEventTypeStub, EVENT_TYPE_SET_PROGRESS, courseSetProgressCallback);

    sinon.assert.notCalled(courseFinishedCallback);
    sinon.assert.notCalled(courseStartedAgainCallback);
    sinon.assert.notCalled(courseSavedInCourseBuilderCallback);
    sinon.assert.notCalled(courseSetProgressCallback);
});

describe('_addListenerForEventType', () => {
    it('has old registered listener', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');
        let windowAddListenerStub = sinon.stub(window, 'addEventListener');
        let getListenerSpy = sinon.spy(ApiIntegrationEventsReceiver, '_getListenerForSetDataEventTypes');

        let oldListener = sinon.stub();
        let eventType = EVENT_TYPE_COURSE_STARTED_AGAIN;
        let newCallback = sinon.stub();
        ApiIntegrationEventsReceiver.registeredListeners = {
            [eventType]: oldListener,
        };

        // @ts-ignore
        ApiIntegrationEventsReceiver._addListenerForEventType(eventType, newCallback);

        sinon.assert.calledOnce(windowRemoveListenerStub);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', oldListener);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toEqual(oldListener);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toBeUndefined();
        sinon.assert.calledOnce(getListenerSpy);
        sinon.assert.calledOnce(windowAddListenerStub);
        sinon.assert.calledWith(windowAddListenerStub, 'message', sinon.match.func);
    });

    it('does not have old registered listener', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');
        let windowAddListenerStub = sinon.stub(window, 'addEventListener');
        let getListenerSpy = sinon.spy(ApiIntegrationEventsReceiver, '_getListenerForSetDataEventTypes');

        let eventType = EVENT_TYPE_COURSE_STARTED_AGAIN;
        let newCallback = sinon.stub();
        ApiIntegrationEventsReceiver.registeredListeners = {};

        // @ts-ignore
        ApiIntegrationEventsReceiver._addListenerForEventType(eventType, newCallback);

        sinon.assert.notCalled(windowRemoveListenerStub);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toBeUndefined();
        sinon.assert.calledOnce(getListenerSpy);
        sinon.assert.calledOnce(windowAddListenerStub);
        sinon.assert.calledWith(windowAddListenerStub, 'message', sinon.match.func);
    });
});

describe('registerGetProgressEvent', () => {
    it('has old registered listener', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');
        let windowAddListenerStub = sinon.stub(window, 'addEventListener');
        let getListenerSpy = sinon.spy(ApiIntegrationEventsReceiver, '_getListenerForGetDataEventTypes');

        let oldListener = sinon.stub();
        let eventType = EVENT_TYPE_GET_PROGRESS;
        let newCallback = sinon.stub();
        ApiIntegrationEventsReceiver.registeredListeners = {
            [eventType]: oldListener,
        };

        ApiIntegrationEventsReceiver.registerGetProgressEvent(newCallback);

        sinon.assert.calledOnce(windowRemoveListenerStub);
        sinon.assert.calledWith(windowRemoveListenerStub, 'message', oldListener);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toEqual(oldListener);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toBeUndefined();
        sinon.assert.calledOnce(getListenerSpy);
        sinon.assert.calledOnce(windowAddListenerStub);
        sinon.assert.calledWith(windowAddListenerStub, 'message', sinon.match.func);
    });

    it('does not have old registered listener', () => {
        let windowRemoveListenerStub = sinon.stub(window, 'removeEventListener');
        let windowAddListenerStub = sinon.stub(window, 'addEventListener');
        let getListenerSpy = sinon.spy(ApiIntegrationEventsReceiver, '_getListenerForGetDataEventTypes');

        let eventType = EVENT_TYPE_GET_PROGRESS;
        let newCallback = sinon.stub();
        ApiIntegrationEventsReceiver.registeredListeners = {};

        ApiIntegrationEventsReceiver.registerGetProgressEvent(newCallback);

        sinon.assert.notCalled(windowRemoveListenerStub);
        // @ts-ignore
        expect(ApiIntegrationEventsReceiver.registeredListeners[eventType]).not.toBeUndefined();
        sinon.assert.calledOnce(getListenerSpy);
        sinon.assert.calledOnce(windowAddListenerStub);
        sinon.assert.calledWith(windowAddListenerStub, 'message', sinon.match.func);
    });
});

describe('_getListenerForSetDataEventTypes', () => {
    it('with correct event name', () => {
        let eventType = EVENT_TYPE_SET_PROGRESS;
        let callback = sinon.stub();
        let event = {
            data: {
                type: eventType,
                data: {
                    field_1: 'some value',
                },
            },
        };

        ApiIntegrationEventsReceiver._getListenerForSetDataEventTypes(eventType, callback)(event);

        sinon.assert.calledOnce(callback);
        sinon.assert.calledWith(callback, event.data.data);
    });

    it('with incorrect event name', () => {
        let eventType = EVENT_TYPE_SET_PROGRESS;
        let callback = sinon.stub();
        let event = {
            data: {
                type: EVENT_TYPE_COURSE_STARTED_AGAIN,
                data: {
                    field_1: 'some value',
                },
            },
        };

        ApiIntegrationEventsReceiver._getListenerForSetDataEventTypes(eventType, callback)(event);

        sinon.assert.notCalled(callback);
    });
});


describe('_getListenerForGetDataEventTypes', () => {
    it('with correct event name', () => {
        let eventType = EVENT_TYPE_GET_PROGRESS;
        let getDataCallbackResult = {
            some_field: 'some field value',
        };
        let callback = sinon.stub().returns(getDataCallbackResult);

        let event = {
            data: {
                type: eventType,
                data: {
                    field_1: 'some value',
                },
            },
            source: {
                postMessage: sinon.stub(),
            }
        };

        ApiIntegrationEventsReceiver._getListenerForGetDataEventTypes(eventType, callback)(event);

        sinon.assert.calledOnce(callback);
        sinon.assert.calledWith(callback, event.data.data);
        sinon.assert.calledWith(event.source.postMessage, {type: eventType, value: getDataCallbackResult}, '*');
    });

    it('with incorrect event name', () => {
        let eventType = EVENT_TYPE_GET_PROGRESS;
        let getDataCallbackResult = {
            some_field: 'some field value',
        };
        let callback = sinon.stub().returns(getDataCallbackResult);

        let event = {
            data: {
                type: EVENT_TYPE_COURSE_STARTED_AGAIN,
                data: {
                    field_1: 'some value',
                },
            },
            source: {
                postMessage: sinon.stub(),
            }
        };

        ApiIntegrationEventsReceiver._getListenerForGetDataEventTypes(eventType, callback)(event);

        sinon.assert.notCalled(callback);
        sinon.assert.notCalled(event.source.postMessage);
    });
});
