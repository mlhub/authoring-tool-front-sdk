import {
    EVENT_TYPE_COURSE_FINISHED,
    EVENT_TYPE_COURSE_SAVED_IN_COURSE_BUILDER,
    EVENT_TYPE_COURSE_STARTED_AGAIN, EVENT_TYPE_GET_PROGRESS, EVENT_TYPE_SET_PROGRESS,IS_LMS,LOAD_PROGRESS_FROM_LMS
} from "./constants";

export default class ApiIntegrationEventsReceiver {
    static registeredListeners = {};

    /**
     * @param callback should be a function to receive SingleCourseProgress param. e.g. (progress) => {// do something}
     */
    static registerCourseStartedAgainEvent(callback) {
        ApiIntegrationEventsReceiver._addListenerForEventType(EVENT_TYPE_COURSE_STARTED_AGAIN, callback);
    }

    /**
     * @param callback should be a function to receive SingleCourseProgress param. e.g. (progress) => {// do something}
     */
    static registerCourseFinishedEvent(callback) {
        ApiIntegrationEventsReceiver._addListenerForEventType(EVENT_TYPE_COURSE_FINISHED, callback);
    }

    /**
     * @param callback should be a function to receive Course object param. e.g. (course) => {// do something}
     */
    static registerCourseSavedInCourseBuilderEvent(callback) {
        ApiIntegrationEventsReceiver._addListenerForEventType(EVENT_TYPE_COURSE_SAVED_IN_COURSE_BUILDER, callback);
    }

    /**
     * @param getProgressCallback should be a function to receive Course object param. e.g. (courseId) => {// return progress for current course}
     */
    static registerGetProgressEvent(getProgressCallback) {
        ApiIntegrationEventsReceiver._addListenerForGetDataEvent(EVENT_TYPE_GET_PROGRESS, getProgressCallback);
    }

    /**
     * @param callback should be a function to receive Course object param. e.g. (progress) => {// do something}
     */
    static registerSetProgressEvent(callback) {
        ApiIntegrationEventsReceiver._addListenerForEventType(EVENT_TYPE_SET_PROGRESS, callback);
    }

    static registerIsLMS = function (getProgressCallback) {
        ApiIntegrationEventsReceiver._addListenerForGetDataEvent(IS_LMS, getProgressCallback);
    };

    static loadProgressReceiver = function (getProgressCallback) {
        ApiIntegrationEventsReceiver._addListenerForGetDataEvent(LOAD_PROGRESS_FROM_LMS, getProgressCallback);
    };

    /**
     *
     * @param eventType
     * @param callback
     * @private
     */
    static _addListenerForEventType(eventType, callback) {
        if (ApiIntegrationEventsReceiver.registeredListeners[eventType]) {
            window.removeEventListener('message', ApiIntegrationEventsReceiver.registeredListeners[eventType]);
        }

        let listener = ApiIntegrationEventsReceiver._getListenerForSetDataEventTypes(eventType, callback);
        ApiIntegrationEventsReceiver.registeredListeners[eventType] = listener;
        window.addEventListener('message', listener);
    }

    static _getListenerForSetDataEventTypes(eventType, callback) {
        return (event) => {
            console.debug('post message received in LMS');
            console.debug(event);

            if (event.data.type === eventType) {
                callback(event.data.data);
            }
        }
    }

    static flushAllListeners() {
        for (let type in ApiIntegrationEventsReceiver.registeredListeners) {
            if (!ApiIntegrationEventsReceiver.registeredListeners[type]) {
                continue;
            }

            window.removeEventListener('message', ApiIntegrationEventsReceiver.registeredListeners[type]);
            delete ApiIntegrationEventsReceiver.registeredListeners[type];
        }
    }

    /**
     *
     * @param eventType
     * @param getDataCallback
     * @private
     */
    static _addListenerForGetDataEvent(eventType, getDataCallback) {
        if (ApiIntegrationEventsReceiver.registeredListeners[eventType]) {
            window.removeEventListener('message', ApiIntegrationEventsReceiver.registeredListeners[eventType]);
        }

        let listener = ApiIntegrationEventsReceiver._getListenerForGetDataEventTypes(eventType, getDataCallback);
        ApiIntegrationEventsReceiver.registeredListeners[eventType] = listener;
        window.addEventListener('message', listener);
    }

    static _getListenerForGetDataEventTypes(eventType, callback) {
        return function (event) {
            console.debug('post message received in LMS');
            console.debug(event);
            if (event.data.type === eventType) {
                var data = callback(event.data.data);
                if (data instanceof Promise) {
                    data.then(resolvedData => {
                        console.log('Resolved data:', resolvedData);
                        event.source.postMessage({ type: eventType+resolvedData.scorm_id, value: resolvedData }, '*');
                    }).catch(err => {
                        console.error('Error resolving promise:', err);
                        event.source.postMessage({ type: eventType, value: { error: 'Failed to load data' } }, '*');
                    });
                } else {
                    event.source.postMessage({ type: eventType, value: data }, '*');
                }
            }
        };
    }
}
