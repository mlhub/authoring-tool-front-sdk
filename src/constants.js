export const URL_PARAM_INTEGRATION_TYPE = 'integration_type';

export const INTEGRATION_TYPE_SCORM = 'scorm';
export const INTEGRATION_TYPE_API = 'api';
export const INTEGRATION_TYPE_PREVIEW = 'preview';
export const INTEGRATION_TYPE_LOCAL = 'local';
export const INTEGRATION_TYPE_PUBLIC_LINK = 'public_link';

export const EVENT_TYPE_COURSE_FINISHED = 'course_finished';
export const EVENT_TYPE_COURSE_STARTED_AGAIN = 'course_started_again';
export const EVENT_TYPE_COURSE_SAVED_IN_COURSE_BUILDER = 'course_saved_in_course_builder';
export const EVENT_TYPE_SET_PROGRESS = 'set_progress';
export const EVENT_TYPE_GET_PROGRESS = 'get_progress';
export const IS_LMS = 'isLMS';
export const LOAD_PROGRESS_FROM_LMS = 'loadProgressFromLMS';
